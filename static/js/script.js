/* Copyright 2019 Daniel Gray 
Licensed under the Apache License, Version 2.0 (the 'License');
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an 'AS IS' BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

let request = new XMLHttpRequest();
let config;
let statuslist;
let allup = true;

async function getStatus(title, url) {
  let statuspoint;
  let status = new XMLHttpRequest();
  status.open("GET", `/api/getstatus?url=${url}`, true);
  status.setRequestHeader("RequestingURL", window.location.href);
  status.send();
  status.onload = function() {
    //If we get back 'good', we can show the site as being up
    if (status.responseText == "good") {
      setStatus();
      statuspoint = document.createElement("li");
      statuspoint.innerHTML = `<p>${title} is currently <b style="color: green;">up</b>.</p>`;
      statuslist.appendChild(statuspoint);
    } else {
      //If we didn't recieve 'good', then we can show the site as being down.
      allup = false;
      setStatus();
      statuspoint = document.createElement("li");
      statuspoint.innerHTML = `<p>${title} is currently <b style="color: red;">down</b>.</p>`;
      statuslist.appendChild(statuspoint);
    }
  };
  //If it is timing out show it as down
  status.ontimeout = function() {
    allup = false;
    statuspoint = document.createElement("li");
    statuspoint.innerHTML = `<p>${title} is currently <b>down</b>.</p>`;
    statuslist.appendChild(statuspoint);
  };
}

function setStatus() {
  let statusMessage = document.getElementById("statusMessage");
  if (allup == true) {
    statusMessage.classList.add("allGood");
    statusMessage.innerHTML = "All systems <b>operational</b>";
  } else {
    statusMessage.classList.add("error");
    statusMessage.innerHTML = "Some systems are <b>down</b>";
  }
}

window.onload = function() {
  //Get the settings file
  statuslist = document.getElementById("statuslist");
  request.open("get", "/settings.json");
  request.responseType = "json";
  request.send();

  //When we get our config file through start setting page properties
  request.onload = function() {
    //Set and brand the page
    config = request.response;
    document.title = config.title;
    let date = new Date();
    document.getElementById("year").innerText = date.getFullYear();
    document.getElementById("title").innerText = config.title;
    //Render in the status of each site listed in the settings.
    for (i = 0; i < config.sites.length; i++) {
      getStatus(config.sites[i].title, config.sites[i].url);
    }
  };
};
