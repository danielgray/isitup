/*Copyright 2019 Daniel Gray
Licensed under the Apache License, Version 2.0 (the 'License');
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an 'AS IS' BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var approvedurl string

//Handler to serve all the files
func handler(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, req.URL.Path[1:])
}

//API Endpoint for testing if a site is up
func getstatus(res http.ResponseWriter, req *http.Request) {
	requestingURL := req.Header.Get("RequestingURL")
	//Make sure the URL is the same as the approved URL, to help restrict who uses it.
	if requestingURL == approvedurl {
		url := req.URL.Query().Get("url")
		client := http.Client{
			Timeout: time.Duration(3 * time.Second),
		}
		_, err := client.Get(url)
		res.Header().Set("Content-Type", "text/plain")
		//Restrict usage with CORS
		res.Header().Set("Access-Control-Allow-Origin", approvedurl)
		//If an error occured (site is down or timed out), send back 'bad'
		if err != nil {
			fmt.Fprintf(res, "bad")
		} else {
			//If request was successful (site is up), send back 'good'
			fmt.Fprintf(res, "good")
		}
	} else {
		fmt.Fprintf(res, "Unauthorised")
	}
}

func main() {
	temp, err := ioutil.ReadFile("programurl.txt")
	if err != nil {
		log.Fatal(err)
	}
	approvedurl = string(temp)
	//Set up the routing for the site.
	http.HandleFunc("/", handler)
	http.HandleFunc("/api/getstatus", getstatus)
	err = http.ListenAndServeTLS(":8085", "ssl/cert.pem", "ssl/private.pem", nil)
	fmt.Println("Webserver running.")
	if err != nil {
		log.Fatal("listenandserve: ", err)
	}
}
