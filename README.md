# IsItUp- The simple status page
Welcome to IsItUp! This is an easy to use status page.

## Setup
To set up IsItUp, first download this repository. Then, you will need to set up your
settings.json and programurl.txt files. You can find examples for both of these in the project files.
You will then need to include your SSL certificate and key in an **ssl** folder. The certificate should be called
*cert.pem* and the key should be named *private.pem*.
By default, IsItUp runs on port **8085**.

## IsItUp is free, but you can help out
Feel free to contributing, it really helps out! If you submit a PR I'll be on it as soon as possible.
If you'd like to donate, I'd be wildly greatful. You can donate here- <https://www.patreon.com/danielgray>

Feel free to check out my [site](https://danielgray.me), where you can find all kinds of things.

(IsItUp is licenced under the Apache Licence 2.0)